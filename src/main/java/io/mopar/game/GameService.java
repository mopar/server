package io.mopar.game;

import com.sun.istack.internal.logging.Logger;
import io.mopar.game.core.*;
import io.mopar.game.action.ActionBindings;
import io.mopar.game.core.lua.LuaScriptEngine;
import io.mopar.game.core.profile.ProfileCodec;
import io.mopar.game.core.req.*;
import io.mopar.game.core.res.*;
import io.mopar.game.lua.ActionsLuaModule;
import io.mopar.game.lua.WorldLuaModule;
import io.mopar.game.model.Player;
import io.mopar.game.model.World;
import io.mopar.game.util.ExecutionTimer;

import javax.script.ScriptException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author Hadyn Fitzgerald
 */
public class GameService implements Runnable {

    /**
     * The logger.
     */
    private static final Logger logger = Logger.getLogger(GameService.class);

    /**
     * The executor this service runs on.
     */
    private ExecutorService executor = Executors.newSingleThreadExecutor();

    /**
     * The script engine.
     */
    private LuaScriptEngine scriptEngine = new LuaScriptEngine();

    /**
     * The action bindings.
     */
    private ActionBindings actionBindings = new ActionBindings();

    /**
     * The request dispatcher.
     */
    private RequestDispatcher dispatcher = new RequestDispatcher();

    /**
     * The codec for decoding and encoding player profiles.
     */
    private ProfileCodec profileCodec = new ProfileCodec();

    /**
     * The timer used for monitoring the amount of ticks to run per cycle.
     */
    private ExecutionTimer timer = new ExecutionTimer();

    /**
     * The world.
     */
    private World world = new World();

    /**
     * The flag for if the service was started.
     */
    private boolean started = false;

    /**
     * The flag for if a stop was requested.
     */
    private boolean stopRequested = false;

    /**
     * Constructs a new {@link GameService};
     */
    public GameService() {
        // Register all of the bindings
        scriptEngine.put(new ActionsLuaModule(actionBindings));
        scriptEngine.put(new WorldLuaModule(world));

        // Bind all of the request handlers.
        registerRequestHandlers();
    }

    /**
     * Initializer method; binds all of the request handlers.
     */
    private void registerRequestHandlers() {
        // Bind all of the create entity request handlers
        dispatcher.bind(NewPlayerRequest.class, this::handleNewPlayerRequest);

        // Bind all of the menu action request handlers
        dispatcher.bind(PlayerMenuActionRequest.class, this::handlePlayerActionRequest);

        // Bind all of the script request handlers
        dispatcher.bind(EvalScriptRequest.class, this::handleEvalScriptRequest);

        // Bind all of the service request handlers
        dispatcher.bind(GetWorldTimeRequest.class, this::handleGetWorldTimeRequest);
    }

    /**
     * Worker loop.
     */
    @Override
    public void run() {

        // Reset the timer
        timer.reset();

        for(;;) {
            synchronized (this) {
                if(stopRequested) {
                    break;
                }
            }

            // Dispatch all of the pending requests
            dispatcher.dispatch();

            // Update the world for the amount of elapsed ticks since the last cycle
            int elapsedTicks = timer.sleep(5, 600);
            for(int i = 0; i < elapsedTicks; i++) {
                world.update();
            }
        }

        // Before we halt the server, stop accepting new requests and dispatch all of the queued requests
        dispatcher.setRejectRequests(true);
        dispatcher.dispatch();

        // Update the world one last time
        world.update();
    }

    /**
     * Helper method; evaluates a script.
     *
     * @param script The script to evaluate.
     * @param callback The response callback.
     */
    public void eval(String script, Callback<EvalScriptResponse> callback) {
        submit(new EvalScriptRequest(script), callback);
    }

    /**
     * Submits a new request to the service.
     *
     * @param request The request.
     * @param callback The callback.
     */
    public void submit(Request request, Callback callback) {
        dispatcher.submit(request, callback);
    }

    /**
     * Starts the service.
     */
    public void start() {
        if(started) {
            throw new IllegalStateException("Game service has already been started");
        }

        // Submit this service to the executor
        executor.submit(this);
        started = true;
    }

    /**
     * Shuts the service down gracefully.
     */
    public void shutdown() {
        synchronized (this) {
            stopRequested = true;
        }
        executor.shutdown();
        while(!executor.isTerminated()) {
            try {
                executor.awaitTermination(1000L, TimeUnit.MILLISECONDS);
            } catch (InterruptedException ex) {}
        }
    }

    /**
     * Handles a new player request.
     *
     * @param request The request.
     * @param callback The callback.
     */
    private void handleNewPlayerRequest(NewPlayerRequest request, Callback callback) {
        Player player = new Player();

        // If the request includes the player profile, decode the player profile and update the player with
        // the decoded profile.
        if(request.hasProfileData()) {
            try {
                Profile profile = profileCodec.decode(request.getEncoding(), request.getProfileData());
                profile.update(player);
            } catch (IOException ex) {
                logger.severe("Failed to decode the provided player profile", ex);
                callback.call(new NewPlayerResponse(NewPlayerResponse.INVALID_PROFILE));
                return;
            }
        }

        // Attempt to add the player to the world, if it is full then we cannot go any further.
        if(!world.addPlayer(player)) {
            callback.call(new NewPlayerResponse(NewPlayerResponse.FULL));
            return;
        }

        // Callback that the request was successful
        callback.call(new NewPlayerResponse(NewPlayerResponse.OK));
    }


    /**
     * Handles a player action request.
     *
     * @param request The request.
     * @param callback The callback.
     */
    private void handlePlayerActionRequest(PlayerMenuActionRequest request, Callback callback) {

        // Get the action source, if the source does not exist we cannot go any further.
        Player player = world.getPlayer(request.getPlayerId());
        if(player == null) {
            callback.call(new MenuActionResponse(MenuActionResponse.SOURCE_DOES_NOT_EXIST));
            return;
        }

        // Get the action target, if the target does not exist we cannot go any further.
        Player target = world.getPlayer(request.getTargetPlayerId());
        if(target == null) {
            callback.call(new MenuActionResponse(MenuActionResponse.TARGET_DOES_NOT_EXIST));
            return;
        }

        // Call the specified action, if the action does not exist alert the callback
        if(!actionBindings.callPlayerMenuAction(player, target, request.getOption())) {
            callback.call(new MenuActionResponse(MenuActionResponse.NO_SUCH_ACTION));
            return;
        }

        // Callback that the request was successful
        callback.call(new MenuActionResponse(MenuActionResponse.OK));
    }

    /**
     * Handles a request to evaluate a script.
     *
     * @param request The request.
     * @param callback The callback.
     */
    private void handleEvalScriptRequest(EvalScriptRequest request, Callback callback) {
        try {
            Object result = scriptEngine.eval(request.getScript());

            // Callback that the request was successful and the result of the script
            EvalScriptResponse response = new EvalScriptResponse(EvalScriptResponse.OK);
            response.setResult(result);
            callback.call(response);
        } catch (ScriptException ex) {
            logger.severe("Failed to evaluate script", ex);

            // TODO(sinisoul): How to effectively callback the exception?
            callback.call(new EvalScriptResponse(EvalScriptResponse.ERROR));
        }
    }

    /**
     * Handles a request to get the current world time.
     *
     * @param request The request.
     * @param callback The callback.
     */
    private void handleGetWorldTimeRequest(GetWorldTimeRequest request, Callback callback) {
        callback.call(new GetWorldTimeResponse(world.getTime()));
    }
}

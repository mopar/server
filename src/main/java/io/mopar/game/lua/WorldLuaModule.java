package io.mopar.game.lua;

import io.mopar.game.core.lua.LuaModule;
import io.mopar.game.model.World;

/**
 * @author Hadyn Fitzgerald
 */
public class WorldLuaModule implements LuaModule {

    /**
     * The world.
     */
    private World world;

    /**
     * Constructs a new {@link WorldLuaModule};
     *
     * @param world The world.
     */
    public WorldLuaModule(World world) {
        this.world = world;
    }

    /**
     * Gets the current world time.
     *
     * @return The time.
     */
    public int time() {
        return world.getTime();
    }

    /**
     * Gets the namespace.
     *
     * @return The namespace.
     */
    @Override
    public String getNamespace() {
        return "world";
    }
}

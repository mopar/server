package io.mopar.game.core;

import io.mopar.game.model.Player;

/**
 * @author Hadyn Fitzgerald
 */
public class Profile {

    /**
     * Updates the player with the profile data.
     *
     * @param player The player to update.
     */
    public void update(Player player) {}
}

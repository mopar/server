package io.mopar.game.core.res;

import io.mopar.game.core.Response;

/**
 * Created by hadyn on 6/20/15.
 */
public class NewPlayerResponse extends Response {

    public static final int OK = 0;
    public static final int FULL = 1;
    public static final int INVALID_PROFILE = 2;

    /**
     * The status.
     */
    private int status;

    /**
     * The id of the player.
     */
    private int playerId = -1;

    /**
     * Constructs a new {@link NewPlayerResponse};
     *
     * @param status The response status.
     */
    public NewPlayerResponse(int status) {
        this.status = status;
    }
}

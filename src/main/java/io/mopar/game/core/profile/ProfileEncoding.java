package io.mopar.game.core.profile;

/**
 * @author Hadyn Fitzgerald
 */
public enum ProfileEncoding {

    /**
     * Binary profile encoding.
     */
    BINARY
}

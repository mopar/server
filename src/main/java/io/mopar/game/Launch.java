package io.mopar.game;

import java.io.IOException;

/**
 * Created by hadyn on 6/21/15.
 */
public class Launch {

    public static void main(String... args) throws IOException {
        GameService service = new GameService();
        service.start();
    }
}

package io.mopar.game.model;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * @author Hadyn Fitzgerald
 */
public class Mobile extends Entity {

    /**
     * The states queue.
     */
    private Queue<Integer> states = new ArrayDeque<>();

    /**
     * The steps.
     */
    private Queue<Step> steps = new ArrayDeque<>();

    /**
     * Flag for clipped movement.
     */
    private boolean clipped = true;

    /**
     * Flag for running.
     */
    private boolean running;

    /**
     * Adds a state.
     *
     * @param state The state.
     */
    public void addState(int state) {
        states.add(state);
    }

    /**
     * Gets the next state.
     *
     * @return The next state.
     */
    public int nextState() {
        return states.poll();
    }

    /**
     * Gets if mobile has pending states.
     *
     * @return If the state queue is not empty.
     */
    public boolean hasStates() {
        return !states.isEmpty();
    }

    /**
     * Adds a step.
     *
     * @param step The step.
     */
    public void addStep(Step step) {
        steps.add(step);
    }

    /**
     * Gets the next step.
     *
     * @return The next step or <code>null</code> if the step queue is empty.
     */
    public Step nextStep() {
        return steps.poll();
    }

    /**
     * Gets if the mobile has pending steps.
     *
     * @return If the step queue is not empty.
     */
    public boolean hasSteps() {
        return !steps.isEmpty();
    }

    /**
     * Clears the step queue.
     */
    public void clearSteps() {
        steps.clear();
    }

    /**
     * Sets if the mobile's movement is clipped.
     *
     * @param clipped The clipped flag.
     */
    public void setClipped(boolean clipped) {
        this.clipped = clipped;
    }

    /**
     * Gets if the mobile's movement is clipped.
     *
     * @return The clipped flag.
     */
    public boolean isClipped() {
        return clipped;
    }

    /**
     * Sets if the mobile is running.
     *
     * @param running The running flag.
     */
    public void setRunning(boolean running) {
        this.running = running;
    }

    /**
     * Gets if the mobile is running.
     *
     * @return The running flag.
     */
    public boolean isRunning() {
        return running;
    }
}

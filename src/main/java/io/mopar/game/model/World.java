package io.mopar.game.model;

import io.mopar.game.model.state.StateHandlerBindings;
import io.mopar.game.model.state.States;

/**
 * @author Hadyn Fitzgerald
 */
public class World {

    /**
     * The maximum amount of players allowed to be spawned in the world.
     */
    public static final int PLAYER_CAPCITY = 2000;

    /**
     * The maximum amount of players allowed to be spawned in the world.
     */
    public static final int NPC_CAPACITY = 16384;

    /**
     * The players.
     */
    private EntityList<Player> players = new EntityList<>(PLAYER_CAPCITY);

    /**
     * The NPCs.
     */
    private EntityList<NPC> npcs = new EntityList<>(NPC_CAPACITY);

    /**
     * The state handler bindings.
     */
    private StateHandlerBindings states = new StateHandlerBindings();

    /**
     * The current time.
     */
    private int time;

    /**
     * Adds a player to the world.
     *
     * @param player The player to add.
     * @return If the player was successfully added.
     */
    public boolean addPlayer(Player player) {
        return players.add(player);
    }

    /**
     * Gets a player by its id.
     *
     * @param id The id of the player.
     * @return The player for the specified id or <code>null</code> if the player does not exist.
     */
    public Player getPlayer(int id) {
        return players.get(id);
    }

    /**
     * Updates the world.
     */
    public void update() {
        time++;

        // Update all of the world players
        for(Player player : players) {
            handlePlayerState(player);
            updateMovement(player);
        }

        // Update all of the world NPCs
        for(NPC npc : npcs) {
            updateMovement(npc);
        }
    }

    /**
     * Handles the next queued player state, or if there are queued states the player is said to be {@link States#IDLE}.
     *
     * @param player The player to handle.
     */
    private void handlePlayerState(Player player) {
        int state = player.hasStates() ? player.nextState() : States.IDLE;
        states.handlePlayerState(player, state);
    }

    /**
     * Updates the movement for a mobile.
     *
     * @param mobile The mobile.
     */
    private void updateMovement(Mobile mobile) {
        if(mobile.hasSteps()) {
            Position source = mobile.getPosition();
            Position current = source;

            int steps = mobile.isRunning() ? 2 : 1;
            while(steps-- > 0) {
                Step step = mobile.nextStep();
                mobile.setPosition(current = current.offset(step.asVector()));
            }
        }
    }

    /**
     * Gets the time.
     *
     * @return The time.
     */
    public int getTime() {
        return time;
    }
}

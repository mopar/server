package io.mopar.game.model;

/**
 * @author Hadyn Fitzgerald
 */
public enum Step {

    /**
     * North step.
     */
    NORTH(0, 1),

    /**
     * North east step.
     */
    NORTH_EAST(1, 1),

    /**
     * East step.
     */
    EAST(1, 0),

    /**
     * South east step.
     */
    SOUTH_EAST(1, -1),

    /**
     * South step.
     */
    SOUTH(0, -1),

    /**
     * South west step.
     */
    SOUTH_WEST(-1, -1),

    /**
     * West step.
     */
    WEST(-1, 0),

    /**
     * North west step.
     */
    NORTH_WEST(-1, 1);

    /**
     * The delta x and y values.
     */
    private int dx, dy;


    /**
     * Constructs a new {@link Step};
     *
     * @param dx The delta x value.
     * @param dy The delta y value.
     */
    Step(int dx, int dy) {
        this.dx = dx;
        this.dy = dy;
    }

    /**
     * Gets the step as a vector.
     *
     * @return The vector.
     */
    public Vector asVector() {
        return new Vector(dx, dy);
    }

    public int getDeltaX() {
        return dx;
    }

    public int getDeltaY() {
        return dy;
    }
}
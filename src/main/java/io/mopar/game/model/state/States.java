package io.mopar.game.model.state;

/**
 * @author Hadyn Fitzgerald
 *
 * The default states.
 */
public class States {

    /**
     * The idle entity state.
     */
    public static final int IDLE = 0;

    /**
     * Prevent instantiation;
     */
    private States() {}
}
